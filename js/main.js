import 'babel-polyfill';
import '../html/styles.less';
import Singleton from './Singleton';
import $ from 'jquery';
window.$ = $;

$(function() {

  // reset url on page (re)load
  history.replaceState({}, {}, '/');

  // hide all content on page (re)load
  $('.main-content .page').addClass('hide');

  window.onpopstate = (e) => {
    let { location: { hash : url } } = e && e.currentTarget || {};
    render(url);
  };

  var render = (url) => {
    var params = url.split('/');
    var view = params.length > 2 ? 'default' : url.split('/')[1] || '';
    $('.main-content .page').addClass('hide');
    $('.nav').find('.nav-item').removeClass('selected');

    switch(view) {
      case '':
        $('.main-content .home').removeClass('hide');
        $('.nav-item.home').addClass('selected');

        break;
      case 'generator-view': renderGeneratorPage(); break;
      case 'log-view': renderLogPage(); break;
      default: {
        alert('Number: ' + params[2] + ', Timestamp: ' + new Date(params[3]*1000));
        renderLogPage(); // @todo hacky persist of log view
      }
    };
  };

  var renderGeneratorPage = () => {
    $('.main-content .page.generator-view').removeClass('hide');
    $('.nav-item.generator-view-link').addClass('selected');
    $('.main-content .home').addClass('hide');
    let elem = document.querySelector('.page.generator-view .button-generator');
    elem.addEventListener('click', handleButtonClick);
  };

  var handleButtonClick = (e) => {
    Singleton.Instance.generate().then(json => {
        Singleton.Instance.add(json);
        alert('Number Generated: ' + json.data[0]);
    });

    setTimeout(() => {
      Singleton.Instance.list();
    }, 1000);
  };

  var renderLogPage = () => {
    $('.main-content .page.log-view').removeClass('hide');
    $('.log-view').empty();
    $('.nav-item.log-view-link').addClass('selected');
    $('.main-content .home').addClass('hide');

    Singleton.Instance.list().forEach((item) => {
      let number = item && item.data && item.data[0];
      let timestamp = item && item.completionTime;
      let anchor = `<a class='log-view-item' href='#!/log-view/${number}/${new Date(timestamp)/1000}'>${number}</a>`;
      $('.log-view').append(anchor);
    });
  };

});
