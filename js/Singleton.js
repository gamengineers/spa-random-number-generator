class Singleton {

  constructor() {
     this._data = [];
  }

  static get Instance() {
    if (this.instance == null || this.instance == undefined) {
      this.instance = new Singleton();
    }
    return this.instance;
  }

  add(item) {
    this._data.push(item);
  }

  list() {
    var sorted = this._reorder(this._data);
    return sorted.length ? sorted : [];
  }

  _reorder(store) {
    return store.sort((a, b) => {
      return new Date(b.completionTime)/1000 - new Date(a.completionTime)/1000;
    });
  }

  generate() {
    var self = this;
    let payload = {
        jsonrpc: '2.0',
        method: 'generateIntegers',
        params: {
            apiKey: 'ce754374-f9a8-4900-9b72-316ccbaea16a', // @todo local config file
            n: 1,
            min: 0,
            max: 100,
            replacement: true,
            base: 10
        },
        id: 12345
    };
    return new Promise((resolve, reject) => {
      this.getJSON('https://api.random.org/json-rpc/1/invoke','POST', payload).then((data) => {
        let random = data && data.result && data.result.random;
        resolve(random);
      });
    });
  }

  getJSON(url, methodType, payload) {
    var promiseObj = new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.open(methodType, url, true);
      xhr.send(JSON.stringify(payload));
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
           if (xhr.status === 200) {
              console.log('xhr done successfully');
              var resp = JSON.parse(xhr.responseText);
              resolve(resp);
           } else {
              reject(xhr.status);
              console.log('xhr failed');
           }
        } else {
           console.log('xhr processing going on');
        }
      };
      console.log('request sent succesfully');
    });
    return promiseObj;
  }

}

export default Singleton;
